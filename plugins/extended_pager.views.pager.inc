<?php
/**
 * @file
 * Extended Pager- Definition of the Views pager plugin.
 *
 */

/**
 * Extended Pager plugin handler class.
 *
 * Based on Views 'full pager' handler class, just change layout to match
 * the extended pager.
 */
class extended_pager extends views_plugin_pager_full {
  /**
   * @param array $input
   *   Any extra GET parameters that should be retained, such as exposed
   *   input.
   * @return string
   *   HTML item list
   */
  public function render($input) {
    $pager_theme = views_theme_functions('extended_pager', $this->view, $this->display);
    $output = theme($pager_theme, array(
      'element' => $this->options['id'],
      'parameters' => $input,
    ));
    return $output;
  }
}
